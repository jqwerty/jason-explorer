# Jason Explorer #
## An API explorer app for DHIS2 ##
*... that is badly in need of a new name*

### Getting started with development ###

1. Clone the git repo:
    - `git clone git@bitbucket.org:jqwerty/jason-explorer.git`
2.  Get the bower packages:
    - `cd Jason\ Explorer; bower install`
3. Install corsproxy to circumvent the lack of CORS support in DHIS:
    - `npm install -g corsproxy`
4. Run the proxy to make the app work properly:
    - `corsproxy`
5. Access the pages directly in a browser, or through a dev server

### Contribution guidelines ###

* Use Trello!