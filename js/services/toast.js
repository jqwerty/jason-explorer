(function(angular) {
  'use strict';

  angular.module('jasonBrowser')
    .factory('toast', ['$mdToast',
      function($mdToast) {
        function retry(txt, cb) {
          //noinspection JSUnresolvedFunction,JSValidateTypes
          $mdToast.show(
            $mdToast.simple()
              .content(txt)
              .position("bottom right")
              .highlightAction(true)
              .action("Retry")
          ).then(cb);
        }

        function msg(txt) {
          $mdToast.show(
            $mdToast.simple()
              .content(txt)
              .position("bottom right")
              .hideDelay(1500)
          );
        }

        return {
          msg  : msg,
          retry: retry
        }
      }
    ]);
})(window.angular);