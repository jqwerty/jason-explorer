(function(angular) {
  'use strict';

  angular.module('jasonBrowser')
    .factory('ApiDataService', ['$http', '$log', '$filter', 'config',
      function($http, $log, $filter, config) {
        // Object containing everything - neat! ////////////////////////////////////////////////////////////////////////
        var apiData = {};

        // Internal utility functions //////////////////////////////////////////////////////////////////////////////////

        function log() {
          if(arguments.length < 1) return;
          arguments[0] = '[ApiDataService] ' + arguments[0];
          $log.log.apply(null, arguments);
        }

        function init() {
          for(var k in apiData) {
            if(apiData.hasOwnProperty(k)) {
              delete apiData[k];
            }
          }
        }

        function reset() {
          log('Resetting stored data');
          init();
        }

        // Initialize data on load
        log('Initializing');
        init();

        // Pre-populated API knowledge /////////////////////////////////////////////////////////////////////////////////
        // This array is merged with the resource list from the API
        var apiKnowledge = [
          {
            plural: 'messageConversations',
            doc   : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s15.html'
          },
          {
            displayName: 'Data Value Sets',
            plural     : 'dataValuesSets',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s09.html',
            queryReq   : true
          },
          {
            displayName: 'Current User Profile',
            plural     : 'me/profile',
            doc        : ''
          },
          {
            displayName: 'Events',
            plural     : 'events',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s10.html'
          },
          {
            displayName: 'Forms',
            plural     : 'forms',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s11.html',
            queryReq   : true
          },
          {
            displayName: 'Complete Data Set Registrations',
            plural     : 'completeDataSetRegistrations',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s12.html',
            queryReq   : true
          },
          {
            displayName: 'Data Approvals',
            plural     : 'dataApprovals',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s14.html',
            queryReq   : true
          },
          {
            displayName: 'Dimensions',
            plural     : 'dimensions',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s21.html'
          },
          {
            displayName: 'Analytics',
            plural     : 'analytics',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s21.html',
            queryReq   : true
          },
          {
            displayName: 'Geo Features',
            plural     : 'geoFeatures',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s23.html',
            queryReq   : true
          },
          {
            displayName: 'Random ID',
            plural     : 'system/id',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s26.html',
            store      : false
          },
          {
            displayName: 'System Info',
            plural     : 'system/info',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s26.html'
          },
          {
            displayName: 'Ping',
            plural     : 'system/ping',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s26.html',
            store      : false
          },
          {
            displayName: 'Current User',
            plural     : 'me',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s27.html'
          },
          {
            displayName: 'Unread Messages and Interpretations',
            plural     : 'me/dashboard',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s27.html'
          },
          {
            displayName: 'Inbox',
            plural     : 'me/inbox',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s27.html'
          },
          {
            displayName: 'Current User Access',
            plural     : 'me/authorization',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s27.html'
          },
          {
            displayName: 'Current User Organisation Units',
            plural     : 'me/organisationUnits',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s27.html'
          },
          {
            displayName: 'Current User Data Sets',
            plural     : 'me/dataSets',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s27.html'
          },
          {
            displayName: 'Current User Programs',
            plural     : 'me/programs',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s27.html'
          },
          {
            displayName: 'Current User Data Approval Levels',
            plural     : 'me/dataApprovalLevels',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s27.html'
          },
          {
            displayName: 'System Settings',
            plural     : 'systemSettings',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s28.html'
          },
          {
            displayName: 'System ID',
            plural     : 'configuration/systemId',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s30.html'
          },
          {
            displayName: 'Feedback Recipients',
            plural     : 'configuration/feedbackRecipients',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s30.html'
          },
          {
            displayName: 'Offline Organisation Unit Level',
            plural     : 'configuration/offlineOrganisationUnitLevel',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s30.html'
          },
          {
            displayName: 'Infrastructure Data Elements',
            plural     : 'configuration/infrastructuralDataElements',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s30.html'
          },
          {
            displayName: 'Infrastructure Period Type',
            plural     : 'configuration/infrastructuralPeriodType',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s30.html'
          },
          {
            displayName: 'Self Registration Role',
            plural     : 'configuration/selfRegistrationRole',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s30.html'
          },
          {
            displayName: 'Self Registration Org Unit',
            plural     : 'configuration/selfRegistrationOrgUnit',
            doc        : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s30.html'
          },
          {
            plural: 'dashboards',
            doc   : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s20.html'
          },
          {
            plural  : 'dashboardItems',
            doc     : 'https://www.dhis2.org/doc/snapshot/en/developer/html/ch01s20.html',
            queryReq: true
          },
          {
            plural  : 'interpretationComments',
            queryReq: true
          },
          {
            plural  : 'trackedEntityInstances',
            queryReq: true
          }
        ];

        // Exported functions //////////////////////////////////////////////////////////////////////////////////////////

        var exp = {};

        /**
         * Get the URL to the specified resource type
         * @param {String} resource (optional)
         * @returns {String} URL
         */
        exp.getResourceTypeUrl = function(resource) {
          if(typeof resource == 'string') {
            return config.apiBase + resource;
          }

          return config.apiBase;
        };

        /**
         *
         * @param success A successful callback!
         * @param error A less successful callback - actually completely failed :-(
         */
        exp.loadResourceTypes = function(success, error) {
          log('Loading Resource Types...');
          if(apiData.resourceTypeCount > 0) {
            log('Resource Types were loaded already');
            success();
            return;
          }

          function storeResourceType(res) {
            apiData.resourceTypes[res.plural] = {
              plural   : res.plural,
              name     : res.displayName,
              doc      : res.doc ? res.doc : '',
              manual   : res.manual === true,
              store    : res.store !== false,
              queryReq : res.queryReq === true,
              itemCount: -1
            };
            var o = { name: res.displayName, plural: res.plural, manual: res.manual };
            apiData.resourceTypesSorted.push(o);
            apiData.resourceTypeCount++;
          }

          $http({
            method : 'GET',
            url    : exp.getResourceTypeUrl('resources'),
            cache  : config.ajaxCache,
            headers: {
              'Authorization': 'Basic ' + btoa('admin:district')
            }
          }).then(
            // Success
            function(payload) {
              var data;
              /** @namespace payload.data.resources */ // Fight the WebStorm!
              data = payload.data.resources;

              angular.forEach(apiKnowledge, function(kRes) {
                if(!kRes.hasOwnProperty('plural')) log('Knowledge about unspecified resource:', kRes);

                var found = false;
                angular.forEach(data, function(aRes) {
                  if(aRes.plural === kRes.plural) {
                    found = true;
                    angular.forEach(kRes, function(val, key) {
                      if(key !== 'plural') {
                        aRes[key] = val;
                      }
                    })
                  }
                });
                if(!found) {
                  kRes['manual'] = true;
                  data.push(kRes);
                }
              });

              //data = $filter('orderBy')(data, 'displayName');

              apiData.resourceTypeCount = 0;
              apiData.resourceTypes = {};
              apiData.resourceTypesSorted = [];
              angular.forEach(data, function(res) {
                storeResourceType(res);
              });
              apiData.resourceTypesSorted = $filter('orderBy')(apiData.resourceTypesSorted, 'name');

              log('Resource Types loaded successfully');
              success();
            },
            // Error
            function(e) {
              log('Failed to load Resource Types', e);
              error({
                status    : e.status,
                statusText: e.statusText
              });
            }
          )
          ;
        };

        exp.getResourceTypeCount = function() {
          return apiData.resourceTypeCount;
        };

        exp.getResourceTypes = function() {
          if(apiData.resourceTypeCount > 0) {
            return apiData.resourceTypes;
          }
        };

        exp.loadResourceType = function(resource, success, error) {
          if(apiData.resourceTypeCount === 0) {
            log('Can\'t get resource type from empty list');
            error({
              status    : 409,
              statusText: 'Resource Types must be loaded first'
            });
            return;
          }
          else if(!apiData.resourceTypes.hasOwnProperty(resource)) {
            log('Resource Type ' + resource + ' doesn\'t exist in Resource Types list');
            error({
              status    : 400,
              statusText: 'Unknown Resource Type: ' + resource
            });
            return;
          }
          else if(apiData.resourceTypes[resource].hasOwnProperty('data') && apiData.resourceTypes[resource].store) {
            log('Resource Type ' + apiData.resourceTypes[resource].name + ' is already loaded!');
            if(apiData.resourceTypes[resource].nextPage) {
              log('Next page: ', apiData.resourceTypes[resource].nextPage);
            }
            success();
            return;
          }

          log('Loading Resource Type: ', resource);

          if(apiData.resourceTypes[resource].queryReq) {
            log('Query parameters required to load Resource Type ' + resource);
            error({
              status    : 400,
              statusText: 'Query parameters required'
            });
          }
          else {
            $http({
              method : 'GET',
              url    : exp.getResourceTypeUrl(resource),
              cache  : config.ajaxCache,
              headers: {
                'Authorization': 'Basic ' + btoa('admin:district')
              }
            }).then(
              // Success
              function(payload) {
                var data = payload.data;
                log('Loaded resource type "' + resource + '"');
                if(data.hasOwnProperty('pager')) {
                  /** @namespace data.pager */
                  /** @namespace data.pager.total */
                  apiData.resourceTypes[resource].itemCount = data.pager.total;
                  if(data.pager.nextPage) {
                    var nextPage = data.pager.nextPage;
                    nextPage = nextPage.substr(nextPage.indexOf('?') + 1);
                    apiData.resourceTypes[resource].nextPage = nextPage;
                    log('Stored next page: ' + nextPage);
                  }
                  else {
                    delete apiData.resourceTypes[resource].nextPage;
                  }
                }
                else if(data.hasOwnProperty(resource)) {
                  apiData.resourceTypes[resource].itemCount = data[resource].length;
                }
                else {
                  apiData.resourceTypes[resource].itemCount = -1;
                }

                apiData.resourceTypes[resource].data = [];
                angular.forEach(data[resource], function(o) {
                  apiData.resourceTypes[resource].data.push(o);
                });
                apiData.resourceTypes[resource].json = JSON.stringify(data, null, 1);

                success();
              },
              // Error
              function(e) {
                log('Failed to load Resource Type "' + resource + '"', e);
                error({
                  status    : e.status,
                  statusText: e.statusText
                });
              }
            );
          }
        };

        exp.loadNextPage = function(resource, success, error) {
          if(apiData.resourceTypes[resource].nextPage) {
            log('Fetching next page of Resource Type ' + resource);
            $http({
              method : 'GET',
              url    : config.apiBase + resource + '?' + apiData.resourceTypes[resource].nextPage,
              cache  : config.ajaxCache,
              headers: {
                'Authorization': 'Basic ' + btoa('admin:district')
              }
            }).then(
              // Success
              function(payload) {
                var data = payload.data;

                if(data.hasOwnProperty('pager')) {
                  /** @namespace data.pager */
                  /** @namespace data.pager.total */
                  if(data.pager.nextPage) {
                    var nextPage = data.pager.nextPage;
                    nextPage = nextPage.substr(nextPage.indexOf('?') + 1);
                    apiData.resourceTypes[resource].nextPage = nextPage;
                    log('Stored next page: ' + nextPage);
                  }
                  else {
                    delete apiData.resourceTypes[resource].nextPage;
                  }
                }
                else {
                  log('Wtf? Paging unpaged resource type?');
                }

                angular.forEach(data[resource], function(o) {
                  apiData.resourceTypes[resource].data.push(o);
                });
                apiData.resourceTypes[resource].json = JSON.stringify(apiData.resourceTypes[resource].data, null, 1);
                success();
              },
              // Error
              function(payload) {
                log('Failed to get next page:', payload);
                error();
              }
            );
          }
          else {
            error({
              status    : 400,
              statusText: 'No next page data...'
            });
          }
        };

        exp.loadResourceDetails = function(resource, id, success, error) {
          if(apiData.resourceDetails && apiData.resourceDetails[resource] && apiData.resourceDetails[resource][id]) {
            log('Details for ' + resource + '/' + id + ' was already loaded!');
            success();
          }
          else {
            log('Loading ' + resource + '/' + id);
            $http({
              method : 'GET',
              url    : config.apiBase + resource + '/' + id,
              cache  : config.ajaxCache,
              headers: {
                'Authorization': 'Basic ' + btoa('admin:district')
              }
            }).then(
              // Success
              function(payload) {
                if(typeof apiData.resourceDetails !== 'object') {
                  apiData.resourceDetails = {};
                }
                if(typeof apiData.resourceDetails[resource] !== 'object') {
                  apiData.resourceDetails[resource] = {};
                }
                apiData.resourceDetails[resource][id] = payload.data;
                apiData.resourceDetails[resource][id].json = JSON.stringify(payload.data, null, 2);
                success();
              },
              // Error
              function(payload) {
                log('Failed to get details: ', payload);
                error({ status: 400, statusText: 'Error' });
              }
            );
          }
        };

        exp.getData = function() {
          return apiData;
        };

        exp.resetApiData = reset;

        return exp;
      }
    ])
  ;

})
(window.angular);