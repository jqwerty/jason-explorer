(function(angular) {
  'use strict';

  angular.module('jasonBrowser')
    .filter('filterResourceType', function() {
      return function(input, match) {
        var output = {};

        if(typeof match != 'string') {
          match = '';
        }
        else {
          match = match.toLowerCase();
        }

        for(var prop in input) {
          if(input.hasOwnProperty(prop)) {
            var
              name = input[prop].name.toLowerCase(),
              plural = input[prop].plural;
            if(name.indexOf(match) != -1 || plural.indexOf(match) != -1) {
              output[prop] = input[prop];
            }
          }
        }

        return output;
      }
    });
})(window.angular);