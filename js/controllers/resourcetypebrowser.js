(function(angular) {
  'use strict';

  var app = angular.module('jasonBrowser');

  /**
   * The ResourceTypeBrowserController is responsible for managing the browsing of resources
   * of a specific resource type.
   *
   * This controller must be nested within a ResourceTypeController scope.
   */
  app.controller("ResourceTypeBrowserController", ['$scope',
    function($scope) {

    }
  ]);

})(window.angular);