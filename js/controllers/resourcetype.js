(function(angular) {
  'use strict';

  var app = angular.module('jasonBrowser');

  /**
   * The ResourceTypeController is responsible for managing a specific resource type.
   *
   * This controller must be nested within an ApiBrowserController scope.
   */
  app.controller("ResourceTypeController", ['$scope', '$log',
    function($scope, $log) {
      $scope.data = $scope.$parent.data;
      $scope.status = $scope.$parent.status;

      // Utility
      function log() {
        if(arguments.length < 1) return;
        arguments[0] = '[ResourceTypeController] ' + arguments[0];
        $log.log.apply(null, arguments);
      }

      // Init
      log('Initializing');

      $scope.getResourcesOfCurrentType = function() {
        log('Fetching resources of type ' + $scope.status.activeResourceType);
        return $scope.data.resourceTypes[$scope.status.activeResourceType].data;
      };

      $scope.getResourceDetails = function(id) {
        log('Fetching details about resource ' + $scope.status.activeResourceType + '/' + id);
        $scope.$parent.getResourceItem($scope.status.activeResourceType, id);
      };

      $scope.toggleJson = function() {
        $scope.status.showPlainJson = !$scope.status.showPlainJson;
      };

      $scope.getNextPage = function() {
        $scope.$parent.getNextPage();
      }
    }
  ]);

})(window.angular);