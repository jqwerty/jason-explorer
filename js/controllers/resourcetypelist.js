(function(angular) {
  'use strict';

  /**
   * The ResourceTypeListController is responsible for managing the list of resource types.
   *
   * This controller must be nested within an ApiBrowserController scope.
   */
  angular.module('jasonBrowser')
    .controller("ResourceTypeListController", ['$scope', '$log',
      function($scope, $log) {
        $scope.data = $scope.$parent.data;
        $scope.status = $scope.$parent.status;

        // Utility
        function log() {
          if(arguments.length < 1) return;
          arguments[0] = '[ResourceTypeListController] ' + arguments[0];
          $log.log.apply(null, arguments);
        }

        $scope.pickResourceType = function(type) {
          log('Set active Resource Type: ', type);
          if(type !== $scope.status.activeResourceType) {
            $scope.data.currentDetails = null;
          }
          $scope.status.activeResourceType = type;
          $scope.$parent.closeSidenav();
          $scope.getResourceType(type);
        };

        // Init
        log('Initializing');
      }
    ]);

})(window.angular);