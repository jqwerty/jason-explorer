(function(angular) {
  'use strict';

  var app = angular.module('jasonBrowser');

  /**
   * Controls the app UI
   */
  app.controller("ApiBrowserController", ['$scope', '$http', '$mdSidenav', '$filter', '$log', 'config', 'ApiDataService', 'toast',
    function($scope, $http, $mdSidenav, $filter, $log, config, apiData, toast) {
      // Fields!
      $scope.data = {};
      $scope.status = {};
      $scope.version = {};

      // Utility
      function log() {
        if(arguments.length < 1) return;
        arguments[0] = '[ApiBrowserController] ' + arguments[0];
        $log.log.apply(null, arguments);
      }

      function init() {
        log('Initializing data');

        $scope.data = apiData.getData();

        $scope.status = {
          activeResourceType: '',
          activeResourceId  : '',
          currentTab        : 0,
          detailsTabEnabled : false,
          filter            : {
            resourceType   : '',
            showManualTypes: true
          },
          loading           : {
            resource       : false,
            resourceType   : false,
            resourceTypes  : false,
            nextPage       : false,
            resourceDetails: false
          }
        };

        $scope.version = config.version;
      }

      $scope.debug = function() {
        log('Angular:', angular);
        log('Version:', $scope.version);
        log('Scope:', $scope);
        log('Status:', $scope.status);
        log('Data:', $scope.data);
      };

      $scope.clear = function() {
        var prop;

        log('Resetting app state');
        apiData.resetApiData();

        $scope.status.activeResourceType = '';
        for(prop in $scope.status.filter) {
          if($scope.status.filter.hasOwnProperty(prop)) {
            $scope.status.filter[prop] = '';
          }
        }
        for(prop in $scope.status.loading) {
          if($scope.status.loading.hasOwnProperty(prop)) {
            $scope.status.loading[prop] = false;
          }
        }
      };

      $scope.getNgVer = function() {
        return angular.version;
      };

      $scope.getResourceTypeList = function() {
        log('Loading Resource Types');
        $scope.status.loading.resourceTypes = true;
        $scope.status.currentTab = 0;
        $scope.status.detailsTabEnabled = true;
        apiData.loadResourceTypes(
          // Success
          function() {
            log('Resource Types loaded successfully');
            $scope.status.loading.resourceTypes = false;
            $scope.openSidenav();
            toast.msg("Loaded " + apiData.getResourceTypeCount() + " resourceTypes");
          },
          // Error
          function() {
            log('Failed to load Resource Types');
            $scope.status.loading.resourceTypes = false;
            toast.retry("Failed to load resource types", function() {
              $scope.getResourceTypeList();
            });
          }
        );
      };

      $scope.getResourceType = function(plural) {
        log('Loading Resource Type: ' + plural);
        $scope.status.loading.resourceType = true;
        $scope.status.currentTab = 0;
        $scope.status.detailsTabEnabled = true;
        apiData.loadResourceType(
          plural,
          // Success
          function() {
            log('Resource Type "' + plural + '" loaded!');
            $scope.status.loading.resourceType = false;
          },
          // Error
          function(e) {
            log('Failed to load resource type "' + plural + '"');
            if(e.statusText) {
              toast.msg('Error loading ' + $scope.data.resourceTypes[plural].name + ': ' + e.status + ' ' + e.statusText);
            }
            $scope.status.loading.resourceType = false;
          });
      };

      $scope.getNextPage = function() {
        $scope.status.loading.nextPage = true;
        apiData.loadNextPage(
          $scope.status.activeResourceType,
          function() {
            $scope.status.loading.nextPage = false;
          },
          function(e) {
            $scope.status.loading.nextPage = false;
            toast.msg('Failed to next page: ' + e.statusText);
          }
        );
      };

      $scope.getResourceItem = function(resource, id) {
        if($scope.status.activeResourceId == id) {
          $scope.status.activeResourceId = '';
        }
        else {
          log('Get Resource Item: ' + resource + '/' + id);
          $scope.status.activeResourceId = id;
          $scope.status.loading.resourceDetails = true;
          apiData.loadResourceDetails(
            resource,
            id,
            function() {
              $scope.status.loading.resourceDetails = false;
              $scope.status.currentTab = 1;
              $scope.status.detailsTabEnabled = true;
              $scope.data.currentDetails = $scope.data.resourceDetails[resource][id];
            },
            function(e) {
              $scope.status.loading.resourceDetails = false;
              $scope.status.detailsTabEnabled = false;
              toast.msg('Failed to load resource details: ' + e.statusText);
            }
          );
        }
      };

      $scope.getPageTitle = function() {
        return ($scope.status.activeResourceType ? $scope.data.resourceTypes[$scope.status.activeResourceType].name + ' | ' : '') + 'JsonExplorer';
      };


      $scope.toggleSidenav = function() {
        $mdSidenav('nav').toggle();
      };

      $scope.openSidenav = function() {
        $mdSidenav('nav').open();
      };

      $scope.closeSidenav = function() {
        $mdSidenav('nav').close();
      };

      // Initialization
      init();
      $scope.getResourceTypeList();
    }
  ])
})(window.angular);