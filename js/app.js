(function(angular) {
  'use strict';

  var config = {
    // Development config
    dev    : {
      ajaxCache: false,
      apiBase  : 'http://localhost:9292/inf5750-15.uio.no/api/'
    },
    // Production config
    prod   : {
      ajaxCache: true,
      apiBase  : 'http://inf5750-15.uio.no/api/'
    },
    active : 'prod',
    version: {
      major: 2,
      minor: 0,
      patch: 0,
      full : '2.0.0'
    }
  };

  // Copy active configuration to default
  for(var attr in config[config.active]) {
    if(config[config.active].hasOwnProperty(attr)) {
      config[attr] = config[config.active][attr];
    }
  }

  angular.module('jasonBrowser', ['ngMaterial'])
    .value('config', config);

})(window.angular);
